# File searching tool

## Purpose

This Python script is designed to take an argument as a file and an argument as
a string and search the file for all instances of the string, ignoring case.

Arguments are as follows:

    -f, --filename  Input file to be searched
    -s, --search    String to be searched using regex

Once a search is complete all found instances for each line are printed in CSV
format with their corresponding line numbers.

## Download

    cd ~/
    git clone https://bitbucket.org/mdill/search-instances.git

## Placement

It is suggested that this script be placed in `/usr/local/bin/` for simplicity
and ease-of-use.  Once this is done, it can be made executable by:

    $ chmod +x /usr/local/bin/search-instances

## License

This project is licensed under the BSD License - see the [LICENSE.md](https://bitbucket.org/mdill/search-instances/src/master/LICENSE.txt) file for
details.

