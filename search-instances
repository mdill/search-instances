#!/usr/bin/env python

import argparse         # Parse arguments
import re               # RegEx for searching

######################################################################
#
# Create arguments for the script
#
######################################################################
parser = argparse.ArgumentParser( prog = "search_instances" )

# Print the version number
parser.add_argument( "--version", action = "version", version = "%(prog)s 0.1" )

requiredNamed = parser.add_argument_group( "required arguments" )
# Get the filename for the file to search
requiredNamed.add_argument( "-f", "--filename", type = str,
        help = "Specify the file to search", required = True )
# Get the string to search for
requiredNamed.add_argument( "-s", "--search", type = str,
        help = "Specify the string to look for", required = True )

args = parser.parse_args()

######################################################################
#
# Child functions
#
######################################################################
def searchText( pos, line ):
    ourSearchString = str( "\w*" + args.search + "\w*" )
    ourReGex = re.compile( ourSearchString, re.IGNORECASE )
    foundReGex = re.findall( ourReGex, line )
    
    if foundReGex:                          # If our ReGex found something
        commaList = ','.join( map( str, foundReGex ) )
        print( str( pos ) + "," + commaList )

######################################################################
#
# Main function
#
######################################################################
def main():
    ourPosition = 1                         # We start on line 1
    with open( args.filename, 'r' ) as ourFile:
        line = ourFile.readline()

        while line:
            searchText( ourPosition, line )
            line = ourFile.readline()
            ourPosition += 1

if __name__ == "__main__":
    main()

